﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ListAndDictionary
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Student student1 = new Student() { ID = 123, Age = 12, Class = "A1", Name = "bang"};
            Student student2 = new Student() { ID = 56, Age = 12, Class = "A1", Name = "long" };
            Student student3 = new Student() { ID = 435, Age = 12, Class = "A1", Name = "me" };

            List<Student> students = new List<Student>();
            students.Add(student1);
            students.Add(student2);
            students.Add(student3);

            // before sort
            foreach (Student student in students) {
                Console.WriteLine(student.ID + " " + student.Class + " " + student.Name + " " + student.Age);
            }
            students.Sort();

            // after  sort
            foreach (Student student in students)
            {
                Console.WriteLine(student.ID + " " + student.Class + " " + student.Name + " " + student.Age);
            }
            var result = student1.FindByID(students, 123);
            Console.WriteLine(result);
            Console.ReadLine();

        }
    }
}
