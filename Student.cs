﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ListAndDictionary
{
    internal class Student : IComparable<Student>
        
    {
        private int _id;

        public int ID
        {
            get { return _id; }
            set { _id = value; }
        }
        private string _name;

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }
        private int _age;

        public int Age
        {
            get { return _age; }
            set { _age = value; }
        }
        private string _class;

        public string Class
        {
            get { return _class; }
            set { _class = value; }
        }

        public int CompareTo(Student other)
        {
            if (this.ID > other.ID) return 1;
            else if (this.ID < other.ID) return -1;
            else return 0;
        }
        public string FindByID(List<Student> students, int ID) {
            int _count = 0;
            foreach (Student student in students) {
                if (student.ID == ID)
                {
                    _count++;                   
                }                
            }
            if (_count < 0) return "Cant find student";
            return "Find a student";

        }
      
    }
}
